import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { z } from 'zod';

@Injectable()
export class ZodValidationPipe
  implements PipeTransform<Record<string, unknown>, Record<string, any>>
{
  constructor(private _schema: z.AnyZodObject) {}

  async transform(
    value: Record<string, unknown>,
    { metatype }: ArgumentMetadata,
  ) {
    if (!metatype) {
      return value;
    }

    const dataOrError = this._schema.safeParse(value);
    if (!dataOrError.success) {
      const error = dataOrError.error;
      throw new BadRequestException(error.format());
    }
    const data = dataOrError.data;
    return data;
  }
}
