import { Body } from '@nestjs/common';
import { z } from 'zod';
import { ZodValidationPipe } from './zod-validator-pipe';

export const ZodValidateBody = (zodSchema: z.AnyZodObject) =>
  Body(new ZodValidationPipe(zodSchema));
