import { InjectionToken, Provider } from '@nestjs/common';
import {
  TGenericOptionFactory,
  TGenericModuleAsyncOptions,
} from './generic-dynamic-module.types';

export function GenericAsyncProviders<T>(PROVIDER_OPTION: string) {
  return class AbstracModule {
    static createAsyncProviders(
      options: TGenericModuleAsyncOptions<T>,
    ): Array<Provider> {
      if (options.useExisting || options.useFactory) {
        return [this.createAsyncOptionsProvider(options)];
      }

      if (options.useClass) {
        return [
          this.createAsyncOptionsProvider(options),
          {
            provide: options.useClass,
            useClass: options.useClass,
          },
        ];
      }
      return [];
    }

    static createAsyncOptionsProvider(
      options: TGenericModuleAsyncOptions<T>,
    ): Provider {
      if (options.useFactory) {
        return {
          provide: PROVIDER_OPTION,
          useFactory: options.useFactory,
          inject: options.inject || [],
        };
      }

      const inject: Array<InjectionToken> = [];
      if (options.useClass) {
        inject.push(options.useClass);
      }
      if (options.useExisting) {
        inject.push(options.useExisting);
      }
      return {
        provide: PROVIDER_OPTION,
        useFactory: async (factoryOptions: TGenericOptionFactory<T>) =>
          await factoryOptions.createOptions(),
        inject,
      };
    }
  };
}
