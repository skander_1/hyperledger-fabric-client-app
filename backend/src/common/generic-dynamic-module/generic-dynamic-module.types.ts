import {
  InjectionToken,
  ModuleMetadata,
  OptionalFactoryDependency,
} from '@nestjs/common/interfaces';

import { Type } from '@nestjs/common';

export type TGenericOptionFactory<T> = {
  createOptions(): Promise<T> | T;
};

export type TGenericModuleAsyncOptions<T> = Pick<ModuleMetadata, 'imports'> & {
  inject?: Array<InjectionToken | OptionalFactoryDependency>;
  useExisting?: Type<TGenericOptionFactory<T>>;
  useClass?: Type<TGenericOptionFactory<T>>;
  useFactory?: (...args: Array<any>) => Promise<T> | T;
};
