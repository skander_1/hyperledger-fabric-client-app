export type Json = boolean | number | string | null | JsonArray | JsonRecord;

interface JsonRecord {
  readonly [key: string]: Json;
}

type JsonArray = ReadonlyArray<Json>;

/**
 * Json Utility type
 * This utilty type will map any type to Json type
 */

export type MapToJsonType<T, U extends JsonSubKeys<T>> = {
  [Property in keyof T as Exclude<Property, keyof U>]: T[Property];
} & U;

// Map Object to Json type
type JsonSubKeys<T> = {
  [K in keyof T as K extends keyof T
    ? T[K] extends Json
      ? never
      : K
    : never]: T[K] extends Json ? never : Json;
};
