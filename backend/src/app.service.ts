import { Injectable } from '@nestjs/common';
import { Json } from './common/types/json';
import {
  HyerpeledgerFabricService,
  InjectHyperledgerFabricService,
} from './infrastructure/hyperledger-fabric';

@Injectable()
export class AppService {
  constructor(
    @InjectHyperledgerFabricService()
    private readonly _hyperledgerFabricService: HyerpeledgerFabricService,
  ) {}

  async getContractMetadata(): Promise<Json> {
    const result = this._hyperledgerFabricService.evaluateTransaction<Json>(
      'org.hyperledger.fabric:GetMetadata',
    )();
    return result;
  }
}
