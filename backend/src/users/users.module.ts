import { Module } from '@nestjs/common';
import { KeycloakAdminModuleWrapper } from 'src/infrastructure/module.wrappers';
import { IdentitiesService } from './services/identities-service';

@Module({
  exports: [IdentitiesService],
  imports: [KeycloakAdminModuleWrapper],
  providers: [IdentitiesService],
})
export class UsersModule {}
