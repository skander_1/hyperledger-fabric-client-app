import { Injectable, Logger, OnApplicationBootstrap } from '@nestjs/common';
import { randomUUID } from 'crypto';
import {
  HyerpeledgerFabricService,
  InjectHyperledgerFabricService,
} from 'src/infrastructure/hyperledger-fabric';
import {
  InjectKeycloakAdmin,
  KeycloakAdminService,
} from 'src/infrastructure/keycloak-admin';
import {
  TCreateNewUserProps,
  TKeycloakUserProps,
} from './identities-service.types';

@Injectable()
export class IdentitiesService implements OnApplicationBootstrap {
  private readonly _logger = new Logger(IdentitiesService.name);

  constructor(
    @InjectKeycloakAdmin()
    private readonly _keycloakAdminService: KeycloakAdminService,
    @InjectHyperledgerFabricService()
    private readonly _hyperledgerFabricService: HyerpeledgerFabricService,
  ) {}

  async onApplicationBootstrap() {
    try {
      await this._enrollAdmin();
    } catch (err) {
      this._logger.error(err);
      process.exit(1);
    }
  }

  public async createNewUser(props: TCreateNewUserProps) {
    const userIdentityLabel = randomUUID();
    const mspId = props.registrarIdentity.mspId;
    let keycloakUserId: string;
    // Create Keycloak user
    try {
      const keycloakResult = await this._createKeycloakUser({
        details: {
          mspId: mspId,
          userIdentityLabel: userIdentityLabel,
        },
        password: props.userDetails.password,
        username: `${props.userDetails.username}@${props.registrarIdentity.mspId}`,
        firstName: props.userDetails.firstName,
        lastName: props.userDetails.lastName,
      });
      keycloakUserId = keycloakResult.id;
    } catch (err) {
      return Promise.reject(err);
    }
    // Register and Entroll the user
    try {
      await this._hyperledgerFabricService.registerAndEnrollUser({
        mspId,
        enrollmentAttributes: props.userDetails.enrollmentAttributes,
        enrollmentId: props.userDetails.username,
        enrollmentSecret: props.userDetails.password,
        role: 'client',
        userIdentityLabel: userIdentityLabel,
        registrarIdentityLabel: props.registrarIdentity.identityLabel,
        registrationAttributes: props.userDetails.registrationAttributes,
        affiliation: props.userDetails.affiliation,
      });
    } catch (err) {
      // Rollback if something went wrong when registring and enrolling a new user
      this._logger.error(
        'Something went wrong when registring and enrolling user',
      );
      this._logger.error(err);
      this._logger.log('rolling back');
      await this._keycloakAdminService.deleteUserById(keycloakUserId);
      this._logger.log('User deleted from keycloak');
      throw err;
    }
  }

  private async _enrollAdmin(): Promise<void> {
    try {
      const enrolledAdminUser =
        await this._hyperledgerFabricService.enrollAdminUser({
          enrollmentAttributes: [],
        });
      await this._createKeycloakUser({
        username: enrolledAdminUser.enrollment.enrollmentId,
        details: {
          mspId: enrolledAdminUser.enrollment.mspId,
          userIdentityLabel: enrolledAdminUser.enrollment.userIdentityLabel,
        },
        password: enrolledAdminUser.enrollment.enrollmentSecret,
      });
    } catch (ex: unknown) {
      if (KeycloakAdminService.isUserExistsError(ex)) {
        this._logger.log('User Already added to keycloak');
        return;
      }
      return Promise.reject(ex);
    }
  }

  private async _createKeycloakUser(props: TKeycloakUserProps) {
    return this._keycloakAdminService.createUser({
      username: props.username,
      emailVerified: true,
      attributes: props.details,
      enabled: true,
      credentials: [
        {
          temporary: false,
          type: 'password',
          value: props.password,
        },
      ],
      firstName: props.firstName,
      lastName: props.lastName,
    });
  }
}
