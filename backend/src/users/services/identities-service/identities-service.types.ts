import {
  TEnrollmentAttributes,
  TRegistrationAttributes,
} from 'src/infrastructure/hyperledger-fabric/service';
import { UserRepresentation } from 'src/infrastructure/keycloak-admin';

export type TKeycloakUserProps = Required<
  Pick<UserRepresentation, 'username'>
> &
  Pick<UserRepresentation, 'firstName'> &
  Pick<UserRepresentation, 'lastName'> & {
    password: string;
    details: {
      mspId: string;
      userIdentityLabel: string;
    };
  };

export type TCreateNewUserProps = {
  registrarIdentity: {
    mspId: string;
    identityLabel: string;
  };
  userDetails: {
    password: string;
    username: string;
    firstName: string;
    lastName: string;
    registrationAttributes: TRegistrationAttributes;
    affiliation: string;
    enrollmentAttributes: TEnrollmentAttributes;
  };
};
