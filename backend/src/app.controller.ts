import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import {
  AuthenticatedUser,
  Protected,
  TDecodedUserToken,
} from './authentication';
import { Json } from './common/types/json';

@Controller('')
export class AppController {
  constructor(private readonly _appService: AppService) {}

  @Get('/metadata')
  @Protected()
  async metatda(
    @AuthenticatedUser() authenticatedUser: TDecodedUserToken,
  ): Promise<Json> {
    return this._appService.getContractMetadata();
  }
}
