import { Module } from '@nestjs/common';

// Modules
import { CommonModule } from './common/common.module';
import { AuthenticationModule } from './authentication';
import { UsersModule } from './users';
import { ConfigModule } from '@nestjs/config';
import {
  HyperledgerFabricModuleWrapper,
  RedisModuleWrapper,
  SessionStoreModuleWrapper,
} from './infrastructure/module.wrappers';
// Controllers
import { AppController } from './app.controller';
// Services
import { AppService } from './app.service';

@Module({
  imports: [
    CommonModule,
    ConfigModule.forRoot({
      envFilePath: 'app.env',
    }),
    HyperledgerFabricModuleWrapper,
    RedisModuleWrapper,
    SessionStoreModuleWrapper,
    AuthenticationModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
