import { DynamicModule, Module, Provider } from '@nestjs/common';
import * as KeycloakConnect from 'keycloak-connect';

import {
  GenericAsyncProviders,
  TGenericModuleAsyncOptions,
} from 'src/common/generic-dynamic-module';

import { Keycloak, KeycloakOptions } from './keycloak.types';
import { KEYCLOAK_OPTIONS, KEYCLOAK_SERVICE } from './keycloak.constants';

@Module({})
export class KeycloakModule extends GenericAsyncProviders<KeycloakOptions>(
  KEYCLOAK_OPTIONS,
) {
  static registerAsync(
    options: TGenericModuleAsyncOptions<KeycloakOptions>,
  ): DynamicModule {
    const KeycloakProvider: Provider = {
      provide: KEYCLOAK_SERVICE,
      useFactory: (keycloakOptions: KeycloakOptions): Keycloak => {
        const keycloak = new KeycloakConnect(
          keycloakOptions.options,
          keycloakOptions.config,
        );
        return keycloak;
      },
      inject: [KEYCLOAK_OPTIONS],
    };
    return {
      module: KeycloakModule,
      imports: options.imports || [],
      providers: [...this.createAsyncProviders(options), KeycloakProvider],
      exports: [KeycloakProvider],
    };
  }
}
