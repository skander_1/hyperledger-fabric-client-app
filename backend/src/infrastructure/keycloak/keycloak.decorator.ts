import { Inject } from '@nestjs/common';
import { KEYCLOAK_SERVICE } from './keycloak.constants';

export function InjectKeycloak() {
  return Inject(KEYCLOAK_SERVICE);
}
