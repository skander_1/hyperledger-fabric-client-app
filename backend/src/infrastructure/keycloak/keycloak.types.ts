import * as KeycloakConnect from 'keycloak-connect';
import { Keycloak } from 'keycloak-connect';

export type KeycloakOptions = {
  options: KeycloakConnect.KeycloakOptions;
  config: KeycloakConnect.KeycloakConfig;
};

export { Keycloak };
