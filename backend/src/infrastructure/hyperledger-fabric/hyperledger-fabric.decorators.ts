import { Inject } from '@nestjs/common';
import { HYPERLEDGER_FABRIC_SERVICE } from './hyperledger-fabric.constants';

export function InjectHyperledgerFabricService() {
  return Inject(HYPERLEDGER_FABRIC_SERVICE);
}
