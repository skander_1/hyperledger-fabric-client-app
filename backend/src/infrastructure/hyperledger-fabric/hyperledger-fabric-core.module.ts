import { DynamicModule, Global, Module, Provider } from '@nestjs/common';
import {
  GenericAsyncProviders,
  TGenericModuleAsyncOptions,
} from 'src/common/generic-dynamic-module';
import {
  HYPERLEDGER_FABRIC_OPTIONS,
  HYPERLEDGER_FABRIC_SERVICE,
} from './hyperledger-fabric.constants';
import { THyperledgerFabricOptions } from './hyperledger-fabric.types';
import { HyerpeledgerFabricService } from './service';

@Global()
@Module({})
export class HyperledgerFabricCoreModule extends GenericAsyncProviders<THyperledgerFabricOptions>(
  HYPERLEDGER_FABRIC_OPTIONS,
) {
  static forRootAsync(
    options: TGenericModuleAsyncOptions<THyperledgerFabricOptions>,
  ): DynamicModule {
    const hyperledgerFabricProvider: Provider = {
      provide: HYPERLEDGER_FABRIC_SERVICE,
      useFactory: async (
        HyperledgerFabricOptions: THyperledgerFabricOptions,
      ): Promise<HyerpeledgerFabricService> =>
        await HyerpeledgerFabricService.init(HyperledgerFabricOptions),
      inject: [HYPERLEDGER_FABRIC_OPTIONS],
    };

    const asyncProviders = this.createAsyncProviders(options);
    return {
      module: HyperledgerFabricCoreModule,
      imports: options.imports,
      providers: [...asyncProviders, hyperledgerFabricProvider],
      exports: [hyperledgerFabricProvider],
    };
  }
}
