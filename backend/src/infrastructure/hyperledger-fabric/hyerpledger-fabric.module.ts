import { DynamicModule, Module } from '@nestjs/common';
import { TGenericModuleAsyncOptions } from 'src/common/generic-dynamic-module';
import { HyperledgerFabricCoreModule } from './hyperledger-fabric-core.module';
import { THyperledgerFabricOptions } from './hyperledger-fabric.types';

@Module({})
export class HyperledgerFabricModule {
  public static forRootAsync(
    options: TGenericModuleAsyncOptions<THyperledgerFabricOptions>,
  ): DynamicModule {
    return {
      module: HyperledgerFabricModule,
      imports: [HyperledgerFabricCoreModule.forRootAsync(options)],
    };
  }
}
