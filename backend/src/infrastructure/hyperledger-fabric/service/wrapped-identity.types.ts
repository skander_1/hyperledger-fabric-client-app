import { Identity, Signer } from '@hyperledger/fabric-gateway';

export type Tx509Certificate = {
  credentials: {
    certificate: string;
    privateKey: string;
  };
  mspId: string;
  type: 'X.509';
};

export type TWrappedIdentity = {
  x509Certificate: Tx509Certificate;
  identity: Identity;
  signer: Signer;
};
