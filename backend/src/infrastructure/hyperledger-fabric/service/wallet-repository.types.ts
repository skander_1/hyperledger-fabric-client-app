import { Wallet } from 'fabric-network';
import { WrappedIdentity } from './wrapped-identity';
import { User } from 'fabric-common';

export type WalletRepository = Omit<Wallet, 'put' | 'get'> &
  IWalletRepositoryInterface;

export type IWalletRepositoryInterface = {
  get(userIdentityLabel: string): Promise<WrappedIdentity | undefined>;
  put(
    userIdentityLabel: string,
    wrappedIdentity: WrappedIdentity,
  ): Promise<void>;
  getRegistrarUser(registrarIdentityLabel: string): Promise<User>;
  isUserExists(userIdentityLabel: string): Promise<boolean>;
};
