import { Wallet } from 'fabric-network';
import { WrappedIdentity } from './wrapped-identity';
import { User } from 'fabric-common';
import {
  IWalletRepositoryInterface,
  WalletRepository,
} from './wallet-repository.types';
import { Tx509Certificate } from './wrapped-identity.types';

export class WalletRepositoryConstructor implements IWalletRepositoryInterface {
  constructor(private readonly _wallet: Wallet) {}

  static make(wallet: Wallet): WalletRepository {
    const instance = new WalletRepositoryConstructor(wallet);

    // Override wallet put and get functionalities
    return new Proxy(wallet, {
      get(target, property: keyof Wallet | keyof WalletRepository) {
        switch (property) {
          case 'get':
            return instance.get.bind(instance);
          case 'put':
            return instance.put.bind(instance);
          case 'isUserExists':
            return instance.isUserExists.bind(instance);
          case 'getRegistrarUser':
            return instance.getRegistrarUser.bind(instance);
          default:
            return target[property as keyof Wallet];
        }
      },
    }) as unknown as WalletRepository;
  }

  async get(userIdentityLabel: string): Promise<WrappedIdentity | undefined> {
    const userIdentityOrNone = (await this._wallet.get(userIdentityLabel)) as
      | Tx509Certificate
      | undefined;
    return userIdentityOrNone
      ? WrappedIdentity.makeFromX509Certificate(userIdentityOrNone)
      : undefined;
  }

  async put(userIdentityLabel: string, wrappedIdentity: WrappedIdentity) {
    return this._wallet.put(userIdentityLabel, wrappedIdentity.x509Certificate);
  }

  async isUserExists(userIdentityLabel: string): Promise<boolean> {
    const userIdentityOrNone = (await this._wallet.get(userIdentityLabel)) as
      | Tx509Certificate
      | undefined;
    return Boolean(userIdentityOrNone);
  }

  async getRegistrarUser(registrarIdentityLabel: string): Promise<User> {
    const registrarIdentity = await this._wallet.get(registrarIdentityLabel);
    if (!registrarIdentity) {
      throw new Error(
        `An identity for the registrar user ${registrarIdentityLabel} does not exist in the wallet`,
      );
    }
    const provider = this._wallet
      .getProviderRegistry()
      .getProvider(registrarIdentity.type);
    const registrarUser = await provider.getUserContext(
      registrarIdentity,
      registrarIdentityLabel,
    );
    return registrarUser;
  }
}
