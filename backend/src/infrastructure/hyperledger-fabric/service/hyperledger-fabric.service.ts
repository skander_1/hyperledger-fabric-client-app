import { Logger } from '@nestjs/common';
/*
 * As recommended by https://www.npmjs.com/package/fabric-network
 * fabric-network is only used for wallets
 */
import { connect, Contract } from '@hyperledger/fabric-gateway';
import { Wallets } from 'fabric-network';

import * as FabricCAServices from 'fabric-ca-client';
import { IAttributeRequest } from 'fabric-ca-client';

import { THyperledgerFabricOptions } from '../hyperledger-fabric.types';
import { ConnectionHelper } from './connection-profile';
import JSONIDAdapter from './json-adapter';
import { TConnectionProfile } from './connection-profile.types';
import { Client } from '@grpc/grpc-js';
import { WrappedIdentity } from './wrapped-identity';
import { WalletRepositoryConstructor } from './wallet-repository';
import { WalletRepository } from './wallet-repository.types';
import {
  TCreateSessionProps,
  TEnrollAndCreateNewUserProps,
  TEnrollUserProps,
  TRegisterAndEnrollUserProps,
  TRegisterUserProps,
} from './hyperledger-fabric-service.types';

/**
 * By Default all transactions are made on behalf of appUser
 */
export class HyerpeledgerFabricService {
  private readonly _utf8Decoder = new TextDecoder();
  private readonly _logger = new Logger(HyerpeledgerFabricService.name);

  private constructor(
    private readonly _client: Client,
    private readonly _walletRepository: WalletRepository,
    private readonly _hyperledgerFabricOptions: THyperledgerFabricOptions,
    private readonly _certificateAuthorityService: FabricCAServices,
    private readonly _appWrappedIdentity: WrappedIdentity,
  ) {}

  static async init(
    hyperledgerFabricOptions: THyperledgerFabricOptions,
  ): Promise<HyerpeledgerFabricService> {
    const cp = await ConnectionHelper.loadProfile(
      hyperledgerFabricOptions.connectionProfileFilePath,
    );

    // The gRPC client connection should be shared by all Gateway connections to this endpoint.
    const client = await ConnectionHelper.newGrpcConnection(cp, true);

    // Initialize and Wallet repository
    const wallet = await Wallets.newFileSystemWallet('./wallet');
    const walletRepository = WalletRepositoryConstructor.make(wallet);

    // Load App Identity from id file
    const appIdentity = await this._loadAppIdentity(hyperledgerFabricOptions);

    // Init Certificate Authority
    const certificateAuthorityService =
      HyerpeledgerFabricService._initFabricCaService(cp);

    const service = new HyerpeledgerFabricService(
      client,
      walletRepository,
      hyperledgerFabricOptions,
      certificateAuthorityService,
      appIdentity,
    );

    service._logger.log('Hyperledger Fabric Service Initialized');
    return service;
  }

  evaluateTransaction<T>(...args: Parameters<Contract['evaluateTransaction']>) {
    return async (userIdentityLabel?: string): Promise<T> => {
      // If User Label is passed make the transaction on behalf of that user
      // else make the transaction on behalf of app User
      const wrappedIdentity = userIdentityLabel
        ? await this._getWrappedIdentity(userIdentityLabel)
        : this._appWrappedIdentity;

      const session = await this._createSession({
        identity: wrappedIdentity.identity,
        signer: wrappedIdentity.signer,
      });

      const resultBytes = await session.contract.evaluateTransaction(...args);
      const resultJson = this._utf8Decoder.decode(resultBytes);
      session.gateway.close();
      return JSON.parse(resultJson) as T;
    };
  }

  submit<T>(...args: Parameters<Contract['submit']>) {
    return async (userIdentityLabel?: string): Promise<T> => {
      // If User Label is passed make the transaction on behalf of that user
      // else make the transaction on behalf of app User
      const wrappedIdentity = userIdentityLabel
        ? await this._getWrappedIdentity(userIdentityLabel)
        : this._appWrappedIdentity;

      const session = await this._createSession({
        identity: wrappedIdentity.identity,
        signer: wrappedIdentity.signer,
      });

      const resultBytes = await session.contract.submit(...args);
      const resultJson = this._utf8Decoder.decode(resultBytes);
      session.gateway.close();
      return JSON.parse(resultJson) as T;
    };
  }

  submitTransaction<T>(...args: Parameters<Contract['submitTransaction']>) {
    return async (userIdentityLabel?: string): Promise<T> => {
      // If User Label is passed make the transaction on behalf of that user
      // else make the transaction on behalf of app User
      const wrappedIdentity = userIdentityLabel
        ? await this._getWrappedIdentity(userIdentityLabel)
        : this._appWrappedIdentity;

      const session = await this._createSession({
        identity: wrappedIdentity.identity,
        signer: wrappedIdentity.signer,
      });

      const resultBytes = await session.contract.submitTransaction(...args);
      const resultJson = this._utf8Decoder.decode(resultBytes);
      session.gateway.close();
      return JSON.parse(resultJson) as T;
    };
  }

  async registerAndEnrollUser(props: TRegisterAndEnrollUserProps) {
    await this._registerUser(props);
    const enrolledUser = await this._enrollUser(props);
    return enrolledUser;
  }

  isUserExists(userIdentityLabel: string) {
    return this._walletRepository.isUserExists(userIdentityLabel);
  }

  public async enrollAdminUser(props: {
    enrollmentAttributes: IAttributeRequest[];
  }) {
    const enrolledAminUser = await this._enrollUser({
      enrollmentAttributes: props.enrollmentAttributes,
      enrollmentId: this._hyperledgerFabricOptions.adminEnrollmentId,
      enrollmentSecret: this._hyperledgerFabricOptions.adminEnrollmentSecret,
      userIdentityLabel: this._hyperledgerFabricOptions.adminIdentityLabel,
      mspId: this._hyperledgerFabricOptions.mspId,
    });

    return {
      enrolledAdminUser: enrolledAminUser,
      enrollment: {
        enrollmentAttributes: props.enrollmentAttributes,
        enrollmentId: this._hyperledgerFabricOptions.adminEnrollmentId,
        enrollmentSecret: this._hyperledgerFabricOptions.adminEnrollmentSecret,
        userIdentityLabel: this._hyperledgerFabricOptions.adminIdentityLabel,
        mspId: this._hyperledgerFabricOptions.mspId,
      },
    };
  }

  private async _enrollUser(props: TEnrollUserProps) {
    // If Identity exists return the identity
    const identity = await this._walletRepository.get(props.userIdentityLabel);
    if (identity) {
      return identity;
    }
    // Else create new Identity
    const result = await this._enrollAndCreateNewUser(props);
    return result.identity;
  }

  private async _registerUser(props: TRegisterUserProps) {
    const registrarUser = await this._walletRepository.getRegistrarUser(
      props.registrarIdentityLabel,
    );
    const registredUser = await this._certificateAuthorityService.register(
      {
        enrollmentID: props.enrollmentId,
        enrollmentSecret: JSON.stringify({ secret: props.enrollmentSecret }),
        role: props.role,
        affiliation: props.affiliation,
        attrs: props.registrationAttributes,
      },
      registrarUser,
    );
    return registredUser;
  }

  private async _enrollAndCreateNewUser(props: TEnrollAndCreateNewUserProps) {
    const enrollment = await this._certificateAuthorityService.enroll({
      enrollmentID: props.enrollmentId,
      enrollmentSecret: props.enrollmentSecret,
      attr_reqs: props.enrollmentAttributes,
    });
    const identity = WrappedIdentity.make({
      certificate: enrollment.certificate,
      privateKey: enrollment.key,
      mspId: props.mspId,
    });
    await this._walletRepository.put(props.userIdentityLabel, identity);
    return { enrollment, identity };
  }

  private async _createSession(props: TCreateSessionProps) {
    const gateway = connect({
      client: this._client,
      identity: props.identity,
      signer: props.signer,
      // Default timeouts for different gRPC calls
      evaluateOptions: () => {
        return { deadline: Date.now() + 5000 }; // 5 seconds
      },
      endorseOptions: () => {
        return { deadline: Date.now() + 15000 }; // 15 seconds
      },
      submitOptions: () => {
        return { deadline: Date.now() + 5000 }; // 5 seconds
      },
      commitStatusOptions: () => {
        return { deadline: Date.now() + 60000 }; // 1 minute
      },
    });

    // Get a network instance representing the channel where the smart contract is deployed.
    const network = gateway.getNetwork(
      this._hyperledgerFabricOptions.channelName,
    );

    // Get the smart contract from the network.
    const contract = network.getContract(
      this._hyperledgerFabricOptions.chaincodeName,
    );

    return {
      network,
      contract,
      gateway,
    };
  }

  // Get Identity Service
  private async _getWrappedIdentity(userIdentityLabel: string) {
    const wrappedIdentityOrNone = await this._walletRepository.get(
      userIdentityLabel,
    );
    if (!wrappedIdentityOrNone) {
      throw new Error('Identity Not Found');
    }
    return wrappedIdentityOrNone;
  }

  private static async _loadAppIdentity(
    hyperledgerFabricOptions: THyperledgerFabricOptions,
  ) {
    // Get AppIdentity
    const jsonAdapter = new JSONIDAdapter(
      hyperledgerFabricOptions.appIdentityFilePath,
      hyperledgerFabricOptions.mspId,
    );

    const jsonId = await jsonAdapter.getStandardJSONID();
    const wrappedIdentity = WrappedIdentity.makeFromStandardJsonId(jsonId);
    return wrappedIdentity;
  }

  private static _initFabricCaService(
    cp: TConnectionProfile,
  ): FabricCAServices {
    return new FabricCAServices(
      cp.certificateAuthorities[Object.keys(cp.certificateAuthorities)[0]].url,
    );
  }
}
