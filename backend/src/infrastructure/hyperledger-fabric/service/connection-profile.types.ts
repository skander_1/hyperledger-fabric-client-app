export type TConnectionProfile = {
  display_name: string;
  id: string;
  name: string;
  type: string;
  version: string;
  certificateAuthorities: any;
  client: any;
  oprganizations: any;
  peers: { [key: string]: TPeer };
};

export type TPeer = {
  grpcOptions: TGrpcOptions;
  url: string;
  tlsCACerts: any;
};

export type TGrpcOptions = {
  'ssl-Target-Name-Override'?: string;
  hostnameOverride?: string;
  'grpc.ssl_target_name_override'?: string;
  'grpc.default_authority'?: string;
};
