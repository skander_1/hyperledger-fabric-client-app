import * as path from 'path';
import * as crypto from 'crypto';
import * as fsPromise from 'fs/promises';
import { Identity, Signer, signers } from '@hyperledger/fabric-gateway';
import { JSONID, TStandardJSONID } from './json-adapter.types';

/**
 * This class can be used to map identities in a variety of JSON formats to the Identity and Signers required
 * for the gateway. For example if you have an application wallet, or have exported IDs from SaaS
 *
 * ```
 *   const jsonAdapter: JSONIDAdapter = new JSONIDAdapter(path.resolve(__dirname,'..','wallet'))
 *
 *   const gateway = connect({
 *       client,
 *       identity: await jsonAdapter.getIdentity("appuser"),
 *       signer: await jsonAdapter.getSigner("appuser"),
 *   });
 *  ```
 *
 * Though they are JSON files, typically they files will have the .id extension. Therefore
 * if no extension is provided `.id` is added
 */
export default class JSONIDAdapter {
  private idFilePath: string;

  /**
   * @param idFilesDir Directory to load the files from
   * @param mspId optional MSPID to apply to all identities returned if they are missing it
   */
  public constructor(idFilePath: string, private mspId: string) {
    this.idFilePath = path.resolve(idFilePath);

    if (mspId) {
      this.mspId = mspId;
    }
  }

  private async readIDFile(idFile: string): Promise<JSONID> {
    let idJsonFile = path.resolve(this.idFilePath);

    // check if there's no extension probably means it's a waller id file
    if (path.extname(idJsonFile) === '') {
      idJsonFile = `${idJsonFile}.id`;
    }

    let id: JSONID;
    const json = JSON.parse(await fsPromise.readFile(idJsonFile, 'utf-8'));

    // look for the nested credentials element
    const credentials = json['credentials'];

    if (credentials) {
      // v2 SDK Wallet format
      id = {
        name: idFile,
        cert: credentials['certificate'],
        ca: '',
        hsm: false,
        private_key: credentials['privateKey'],
        mspId: json.mspId,
      };
    } else {
      // IBP exported ID style format
      id = {
        name: json.name,
        cert: Buffer.from(json.cert, 'base64').toString(),
        ca: Buffer.from(json.ca, 'base64').toString(),
        hsm: json.js,
        private_key: Buffer.from(json.private_key, 'base64').toString(),
        mspId: this.mspId,
      };
    }
    return id;
  }

  /**
   *
   * @param id Loaded identity file
   * @returns Identity to use with the GatewayBuilder
   */
  public async getIdentity(id: JSONID): Promise<Identity> {
    const identity: Identity = {
      credentials: Buffer.from(id.cert),
      mspId: id.mspId,
    };

    return identity;
  }

  /**
   *
   * @param id Loaded identity file
   * @returns Signer to use with the GatewayBuilder
   */
  public async getSigner(id: JSONID): Promise<Signer> {
    const privateKey = crypto.createPrivateKey(this._getPrivateKey(id));
    return signers.newPrivateKeySigner(privateKey);
  }

  private _getPrivateKey(id: JSONID) {
    if ('private_key' in id && id.private_key) {
      return id.private_key;
    } else if ('privateKey' in id && id.privateKey) {
      return id['privateKey'];
    }
    throw new Error('Unable to parse the identity json file');
  }

  public async getStandardJSONID(): Promise<TStandardJSONID> {
    const id = await this.readIDFile(this.idFilePath);
    return {
      ...id,
      privateKey: this._getPrivateKey(id),
    };
  }
}
