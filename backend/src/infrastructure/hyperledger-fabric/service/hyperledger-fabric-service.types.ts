import { Signer, Identity } from '@hyperledger/fabric-gateway';
import { IAttributeRequest, IKeyValueAttribute } from 'fabric-ca-client';

export type TEnrollmentAttributes = IAttributeRequest[];
export type TRegistrationAttributes = IKeyValueAttribute[];

export type TRegisterAndEnrollUserProps = {
  userIdentityLabel: string;
  registrarIdentityLabel: string;
  enrollmentId: string;
  enrollmentSecret: string;
  registrationAttributes: TRegistrationAttributes;
  mspId: string;
  enrollmentAttributes: TEnrollmentAttributes;
  affiliation: string;
  role: 'client';
};

export type TRegisterUserProps = {
  registrarIdentityLabel: string;
  enrollmentId: string;
  enrollmentSecret: string;
  registrationAttributes: TRegistrationAttributes;
  affiliation: string;
  role: 'client';
};

export type TEnrollAndCreateNewUserProps = {
  userIdentityLabel: string;
  enrollmentId: string;
  enrollmentSecret: string;
  mspId: string;
  enrollmentAttributes: TEnrollmentAttributes;
};

export type TEnrollUserProps = {
  userIdentityLabel: string;
  enrollmentId: string;
  enrollmentSecret: string;
  mspId: string;
  enrollmentAttributes: TEnrollmentAttributes;
};

// Use Identity from Fabric Gateway and not from Fabric-network
export type TCreateSessionProps = {
  identity: Identity;
  signer: Signer;
};

export type TEnrollAdminUserProps = {
  enrollmentAttributes: TEnrollmentAttributes;
};
