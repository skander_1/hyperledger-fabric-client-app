/** Internal interface used to describe all the possible components
 * of the identity
 */
export type JSONID = {
  name: string;
  cert: string;
  ca: string;
  hsm: boolean;
  private_key?: string;
  privateKey?: string;
  mspId: string;
};

export type TStandardJSONID = Omit<JSONID, 'private_key' | 'privateKey'> & {
  privateKey: string;
};
