import { Identity, Signer, signers } from '@hyperledger/fabric-gateway';
import * as crypto from 'crypto';
import { IEnrollResponse } from 'fabric-ca-client';
import { TStandardJSONID } from './json-adapter.types';
import { TWrappedIdentity, Tx509Certificate } from './wrapped-identity.types';

export class WrappedIdentity {
  constructor(private readonly _props: TWrappedIdentity) {}

  get x509Certificate() {
    return this._props.x509Certificate;
  }

  get signer(): Signer {
    return this._props.signer;
  }

  get identity(): Identity {
    return this._props.identity;
  }

  static makeFromStandardJsonId(
    standardJsonId: TStandardJSONID,
  ): WrappedIdentity {
    // Create Signer
    const signer = this._makeSigner({ privateKey: standardJsonId.privateKey });
    const identity = this._makeIdentity({
      certificate: standardJsonId.cert,
      mspId: standardJsonId.mspId,
    });
    // Create Identity
    const x509Certificate = this._makeX509Certificate({
      certificate: standardJsonId.cert,
      mspId: standardJsonId.mspId,
      privateKey: standardJsonId.privateKey,
    });

    return new WrappedIdentity({
      identity,
      signer,
      x509Certificate,
    });
  }

  static makeFromX509Certificate(
    x509Certificate: Tx509Certificate,
  ): WrappedIdentity {
    // Create Signer
    const signer = this._makeSigner({
      privateKey: x509Certificate.credentials.privateKey,
    });
    // Create Identity
    const identity = this._makeIdentity({
      certificate: x509Certificate.credentials.certificate,
      mspId: x509Certificate.mspId,
    });
    return new WrappedIdentity({
      x509Certificate,
      signer,
      identity,
    });
  }

  static make(props: {
    certificate: string;
    privateKey: IEnrollResponse['key'] | string;
    mspId: string;
  }): WrappedIdentity {
    // convert private key if bytes
    const privateKey =
      typeof props.privateKey === 'string'
        ? props.privateKey
        : props.privateKey.toBytes();
    // Create Signer
    const signer = this._makeSigner({ privateKey });
    // Create Identity
    const identity = this._makeIdentity({
      certificate: props.certificate,
      mspId: props.mspId,
    });
    // Create x509 Certificate
    const x509Certificate = this._makeX509Certificate({
      certificate: props.certificate,
      mspId: props.mspId,
      privateKey,
    });

    return new WrappedIdentity({
      x509Certificate,
      signer,
      identity,
    });
  }

  private static _makeSigner(props: { privateKey: string }) {
    const privateKeyObject = crypto.createPrivateKey(props.privateKey);
    const signer = signers.newPrivateKeySigner(privateKeyObject);
    return signer;
  }

  private static _makeIdentity(props: {
    certificate: string;
    mspId: string;
  }): Identity {
    return {
      credentials: Buffer.from(props.certificate),
      mspId: props.mspId,
    };
  }

  private static _makeX509Certificate(props: {
    certificate: string;
    privateKey: string;
    mspId: string;
  }): Tx509Certificate {
    return {
      credentials: {
        certificate: props.certificate,
        privateKey: props.privateKey,
      },
      mspId: props.mspId,
      type: 'X.509',
    };
  }
}
