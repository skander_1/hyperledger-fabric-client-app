export type THyperledgerFabricOptions = {
  channelName: string;
  chaincodeName: string;
  connectionProfileFilePath: string;
  appIdentityFilePath: string;
  mspId: string;
  adminEnrollmentId: string;
  adminEnrollmentSecret: string;
  adminIdentityLabel: string;
};
