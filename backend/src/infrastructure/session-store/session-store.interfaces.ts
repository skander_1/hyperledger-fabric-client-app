import { SessionOptions } from 'express-session';
import * as RedisStoreInit from 'connect-redis';

export type SessionStoreOptions = {
  sessionConfig: Omit<SessionOptions, 'store'>;
  redisClient: RedisStoreInit.Client;
};

export { Store } from 'express-session';
