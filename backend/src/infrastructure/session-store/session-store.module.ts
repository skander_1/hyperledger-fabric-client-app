import { DynamicModule, Module } from '@nestjs/common';
import { TGenericModuleAsyncOptions } from 'src/common/generic-dynamic-module';
import { SessionStoreCoreModule } from './session-store-core.module';
import { SessionStoreOptions } from './session-store.interfaces';

@Module({})
export class SessionStoreModule {
  public static forRootAsync(
    options: TGenericModuleAsyncOptions<SessionStoreOptions>,
  ): DynamicModule {
    return {
      module: SessionStoreModule,
      imports: [SessionStoreCoreModule.forRootAsync(options)],
    };
  }
}
