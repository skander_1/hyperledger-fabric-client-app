import { DynamicModule, Global, Module, Provider } from '@nestjs/common';
import {
  GenericAsyncProviders,
  TGenericModuleAsyncOptions,
} from 'src/common/generic-dynamic-module';

import {
  SESSION_MIDDLEWARE,
  SESSION_STORE_OPTIONS,
  STORE_PROVIDER,
} from './session-store.constants';
import * as session from 'express-session';
import * as RedisStoreInit from 'connect-redis';

import { SessionStoreOptions, Store } from './session-store.interfaces';
import { NextFunction, Request, Response } from 'express';

@Global()
@Module({})
export class SessionStoreCoreModule extends GenericAsyncProviders<SessionStoreOptions>(
  SESSION_STORE_OPTIONS,
) {
  static forRootAsync(
    options: TGenericModuleAsyncOptions<SessionStoreOptions>,
  ): DynamicModule {
    const StoreProvider: Provider = {
      provide: STORE_PROVIDER,
      useFactory: (sessionStoreOptions: SessionStoreOptions): session.Store => {
        const RedisStore = RedisStoreInit(session);
        const store = new RedisStore({
          client: sessionStoreOptions.redisClient,
        });
        return store;
      },
      inject: [SESSION_STORE_OPTIONS],
    };

    const asyncProviders = this.createAsyncProviders(options);

    const SessionMiddleware: Provider = {
      provide: SESSION_MIDDLEWARE,
      useFactory: (
        store: Store,
        sessionStoreOptions: SessionStoreOptions,
      ): ((req: Request, res: Response, next: NextFunction) => void) => {
        return session({
          ...sessionStoreOptions.sessionConfig,
          store,
        });
      },
      inject: [STORE_PROVIDER, SESSION_STORE_OPTIONS],
    };

    return {
      module: SessionStoreCoreModule,
      imports: options.imports,
      providers: [...asyncProviders, StoreProvider, SessionMiddleware],
      exports: [StoreProvider, SessionMiddleware],
    };
  }
}
