import { Inject } from '@nestjs/common';
import { SESSION_MIDDLEWARE, STORE_PROVIDER } from './session-store.constants';

export function InjectStore() {
  return Inject(STORE_PROVIDER);
}

export function InjectSessionMiddleware() {
  return Inject(SESSION_MIDDLEWARE);
}
