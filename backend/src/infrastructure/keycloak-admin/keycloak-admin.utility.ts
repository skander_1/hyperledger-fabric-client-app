import KeycloakAdminClient from '@keycloak/keycloak-admin-client';

const PACKAGE_NAME = '@keycloak/keycloak-admin-client';

/*
    Work Around because Nestjs does not support ES Modules
    https://github.com/keycloak/keycloak-nodejs-admin-client/issues/523
*/
export const importKeyCloaAdminkModule = async () => {
  const importedModuleFn = new Function(`return import('${PACKAGE_NAME}')`);
  const importedModule = await importedModuleFn();
  const defaultEntrypoint =
    importedModule.default as typeof KeycloakAdminClient;
  return defaultEntrypoint;
};

// Trick to keep dependencies into webpack build
const nottrue = false;
if (nottrue) import(PACKAGE_NAME);
