import { UserQuery } from '@keycloak/keycloak-admin-client/lib/resources/users';
import { KeycloakAdminClient } from '@keycloak/keycloak-admin-client/lib/client';
import {
  KeycloakAdminOptions,
  Credentials,
  UserRepresentation,
} from './keycloak-admin.interfaces';
import { importKeyCloaAdminkModule } from './keycloak-admin.utility';
import { Logger } from '@nestjs/common';
import axios from 'axios';

export class KeycloakAdminService {
  private readonly _logger = new Logger(KeycloakAdminService.name, {
    timestamp: true,
  });

  private constructor(
    private readonly _keycloakAdminClient: KeycloakAdminClient,
  ) {}

  static async init(
    ConnectionConfig: KeycloakAdminOptions,
  ): Promise<KeycloakAdminService> {
    // Dynamic import of ESM lib
    const kcAdminClient = await importKeyCloaAdminkModule();

    const keycloakAdminClientInstance = new kcAdminClient(
      ConnectionConfig.connectionConfig,
    );

    const keycloakAdminInstance = new KeycloakAdminService(
      keycloakAdminClientInstance,
    );

    await keycloakAdminInstance._authenticate(ConnectionConfig.credentials);
    keycloakAdminInstance._logger.log('Keycloak Admin Service Initialized');

    setInterval(
      () => keycloakAdminInstance._authenticate(ConnectionConfig.credentials),
      60 * 1000,
    );

    return keycloakAdminInstance;
  }

  private async _authenticate(credentials: Credentials) {
    await this._keycloakAdminClient.auth(credentials);
  }

  updateUser<T extends UserRepresentation>(userId: string, updatedUser: T) {
    return this._keycloakAdminClient.users.update({ id: userId }, updatedUser);
  }

  createUser<T extends UserRepresentation>(user: T) {
    return this._keycloakAdminClient.users.create(user);
  }

  verifyEmail(userId: string) {
    return this._keycloakAdminClient.users.sendVerifyEmail({ id: userId });
  }

  findUser(userQuery: UserQuery & Record<string, string | number>) {
    return this._keycloakAdminClient.users.find(userQuery);
  }

  getUserById(userId: string) {
    return this._keycloakAdminClient.users.findOne({ id: userId });
  }

  deleteUserById(userId: string) {
    return this._keycloakAdminClient.users.del({ id: userId });
  }

  static isUserExistsError(error: unknown) {
    return (
      axios.isAxiosError(error) &&
      error?.response?.status === 409 &&
      typeof error?.response?.data === 'object' &&
      error?.response?.data !== null &&
      'errorMessage' in error?.response?.data &&
      error?.response?.data?.errorMessage === 'User exists with same username'
    );
  }
}
