import { DynamicModule, Module, Provider } from '@nestjs/common';
import {
  GenericAsyncProviders,
  TGenericModuleAsyncOptions,
} from 'src/common/generic-dynamic-module';
import {
  KEYCLOAK_ADMIN_OPTIONS,
  KEYCLOAK_ADMIN_SERVICE,
} from './keycloak-admin.constants';

import { KeycloakAdminOptions } from './keycloak-admin.interfaces';
import { KeycloakAdminService } from './keycloak-admin.service';

@Module({})
export class KeycloakAdminModule extends GenericAsyncProviders<KeycloakAdminOptions>(
  KEYCLOAK_ADMIN_OPTIONS,
) {
  static registerAsync(
    options: TGenericModuleAsyncOptions<KeycloakAdminOptions>,
  ): DynamicModule {
    const KeycloakAdminProvider: Provider = {
      provide: KEYCLOAK_ADMIN_SERVICE,
      useFactory: async (keycloakAdminOptions: KeycloakAdminOptions) => {
        const keycloakService = KeycloakAdminService.init(keycloakAdminOptions);
        return keycloakService;
      },
      inject: [KEYCLOAK_ADMIN_OPTIONS],
    };
    return {
      module: KeycloakAdminModule,
      imports: options.imports || [],
      providers: [...this.createAsyncProviders(options), KeycloakAdminProvider],
      exports: [KeycloakAdminProvider],
    };
  }
}
