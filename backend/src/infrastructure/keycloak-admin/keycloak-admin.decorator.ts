import { Inject } from '@nestjs/common';
import { KEYCLOAK_ADMIN_SERVICE } from './keycloak-admin.constants';

export function InjectKeycloakAdmin() {
  return Inject(KEYCLOAK_ADMIN_SERVICE);
}
