import { ConnectionConfig } from '@keycloak/keycloak-admin-client/lib/client';
import { Credentials } from '@keycloak/keycloak-admin-client/lib/utils/auth';
import UserRepresentation from '@keycloak/keycloak-admin-client/lib/defs/userRepresentation';

export type KeycloakAdminOptions = {
  connectionConfig: ConnectionConfig;
  credentials: Credentials;
};

export { Credentials, ConnectionConfig, UserRepresentation };
