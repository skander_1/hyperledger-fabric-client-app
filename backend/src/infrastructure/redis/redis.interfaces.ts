import { RedisOptions } from 'ioredis';

export type TRedisOptions = RedisOptions;
