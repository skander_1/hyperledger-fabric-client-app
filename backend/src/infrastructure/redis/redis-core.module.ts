import { Global, Module, Provider } from '@nestjs/common';
import {
  GenericAsyncProviders,
  TGenericModuleAsyncOptions,
} from 'src/common/generic-dynamic-module';
import { REDIS_OPTIONS, REDIS_SERVICE } from './redis.constants';
import { TRedisOptions } from './redis.interfaces';
import { RedisService } from './redis.service';

@Global()
@Module({})
export class RedisCoreModule extends GenericAsyncProviders<TRedisOptions>(
  REDIS_OPTIONS,
) {
  static forRootAsync(options: TGenericModuleAsyncOptions<TRedisOptions>) {
    const RedisProvider: Provider = {
      provide: REDIS_SERVICE,
      useFactory: async (
        redisOptions: TRedisOptions,
      ): Promise<RedisService> => {
        const redisInstance = await RedisService.init(redisOptions);
        return redisInstance;
      },
      inject: [REDIS_OPTIONS],
    };

    const asyncProviders = this.createAsyncProviders(options);
    return {
      module: RedisCoreModule,
      imports: options.imports,
      providers: [...asyncProviders, RedisProvider],
      exports: [RedisProvider],
    };
  }
}
