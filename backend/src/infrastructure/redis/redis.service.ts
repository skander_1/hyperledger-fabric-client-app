import { RedisOptions } from 'ioredis';
import IORedis from 'ioredis';
import { Logger } from '@nestjs/common';

export class RedisService {
  private readonly _logger = new Logger(RedisService.name, { timestamp: true });

  private constructor(private readonly _redisClient: IORedis) {}

  getClient() {
    return this._redisClient;
  }

  static async init(redisOptions: RedisOptions): Promise<RedisService> {
    return new Promise((resolve, reject) => {
      const redisClient = new IORedis(redisOptions);

      redisClient.on('connect', () => {
        const redisInstance = new RedisService(redisClient);
        redisInstance._logger.log('Connected to redis');
        return resolve(redisInstance);
      });

      redisClient.on('error', (err) => reject(err));
    });
  }
}
