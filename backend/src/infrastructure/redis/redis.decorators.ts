import { Inject } from '@nestjs/common';
import { REDIS_SERVICE } from './redis.constants';

export function InjectRedis() {
  return Inject(REDIS_SERVICE);
}
