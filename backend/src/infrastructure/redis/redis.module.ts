import { DynamicModule, Module } from '@nestjs/common';
import { RedisOptions } from 'ioredis';
import { TGenericModuleAsyncOptions } from 'src/common/generic-dynamic-module';
import { RedisCoreModule } from './redis-core.module';

@Module({})
export class RedisModule {
  public static forRootAsync(
    options: TGenericModuleAsyncOptions<RedisOptions>,
  ): DynamicModule {
    return {
      module: RedisModule,
      imports: [RedisCoreModule.forRootAsync(options)],
    };
  }
}
