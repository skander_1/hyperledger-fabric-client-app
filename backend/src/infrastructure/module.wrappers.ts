import { ConfigModule, ConfigType } from '@nestjs/config';
import { hyperledgerFabricConfig } from 'src/config/hyperledger-fabric-config';
import { keycloakAdminConfig } from 'src/config/keycloak-admin-config';
import { keycloakConfig } from 'src/config/keycloak-config';
import { HyperledgerFabricModule } from './hyperledger-fabric';
import { KeycloakModule } from './keycloak';
import { KeycloakAdminModule } from './keycloak-admin';
import { RedisModule, RedisService, REDIS_SERVICE } from './redis';
import { SessionStoreModule, Store, STORE_PROVIDER } from './session-store';

// Global Modules
export const RedisModuleWrapper = RedisModule.forRootAsync({
  imports: [],
  inject: [],
  useFactory: () => {
    return {};
  },
});

export const SessionStoreModuleWrapper = SessionStoreModule.forRootAsync({
  imports: [RedisModuleWrapper],
  inject: [REDIS_SERVICE],
  useFactory: (_redisService: RedisService) => {
    return {
      redisClient: _redisService.getClient(),
      sessionConfig: {
        secret: '',
        resave: false,
        saveUninitialized: true,
        cookie: {
          sameSite: true,
          httpOnly: true,
          maxAge: 1000 * 60 * 60 * 24 * 30,
        },
      },
    };
  },
});

export const KeycloakModuleWrapper = KeycloakModule.registerAsync({
  imports: [ConfigModule.forFeature(keycloakConfig)],
  inject: [STORE_PROVIDER, keycloakConfig.KEY],
  useFactory: (
    store: Store,
    _keycloakConfig: ConfigType<typeof keycloakConfig>,
  ) => {
    return {
      options: {
        store: store,
      },
      config: _keycloakConfig,
    };
  },
});

export const KeycloakAdminModuleWrapper = KeycloakAdminModule.registerAsync({
  imports: [ConfigModule.forFeature(keycloakAdminConfig)],
  inject: [keycloakAdminConfig.KEY],
  useFactory: (
    _keycloakAdminConfig: ConfigType<typeof keycloakAdminConfig>,
  ) => {
    return {
      connectionConfig: {
        baseUrl: _keycloakAdminConfig.baseUrl,
        realmName: _keycloakAdminConfig.realmName,
      },
      credentials: {
        clientId: _keycloakAdminConfig.clientId,
        clientSecret: _keycloakAdminConfig.clientSecret,
        grantType: _keycloakAdminConfig.grantType,
      },
    };
  },
});

export const HyperledgerFabricModuleWrapper =
  HyperledgerFabricModule.forRootAsync({
    imports: [ConfigModule.forFeature(hyperledgerFabricConfig)],
    inject: [hyperledgerFabricConfig.KEY],
    useFactory: (
      _hyperledgerFabricConfig: ConfigType<typeof hyperledgerFabricConfig>,
    ) => _hyperledgerFabricConfig,
  });
