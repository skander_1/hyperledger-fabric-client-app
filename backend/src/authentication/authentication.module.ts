import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { KeycloakModuleWrapper } from 'src/infrastructure/module.wrappers';
import { AuthenticationGuard } from './authentication.guard';

@Module({
  imports: [KeycloakModuleWrapper],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthenticationGuard,
    },
  ],
})
export class AuthenticationModule {}
