import {
  createParamDecorator,
  ExecutionContext,
  SetMetadata,
} from '@nestjs/common';
import {
  AUTHENTICATED_USER_INFO,
  META_PROTECTED,
} from './authentication.constants';

export const Protected = () => SetMetadata(META_PROTECTED, true);

export const AuthenticatedUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request[AUTHENTICATED_USER_INFO];
  },
);
