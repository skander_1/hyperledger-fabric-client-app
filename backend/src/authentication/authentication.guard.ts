import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { InjectKeycloak, Keycloak, Token } from 'src/infrastructure/keycloak';
import { Request } from 'express';
import {
  TDecodedUserToken,
  TExtendedRequest,
} from './authentication-guard.types';
import {
  AUTHENTICATED_USER_INFO,
  IS_USER_AUTHENTICATED,
  META_PROTECTED,
} from './authentication.constants';
import {
  HyerpeledgerFabricService,
  InjectHyperledgerFabricService,
} from 'src/infrastructure/hyperledger-fabric';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  private readonly _logger: Logger = new Logger(AuthenticationGuard.name, {
    timestamp: true,
  });

  constructor(
    @InjectKeycloak() private readonly _keycloak: Keycloak,
    private readonly _reflector: Reflector,
    @InjectHyperledgerFabricService()
    private readonly _hyperledgerFabricSerice: HyerpeledgerFabricService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const activeGuard = this._reflector.getAllAndOverride<true | undefined>(
      META_PROTECTED,
      [context.getHandler(), context.getClass()],
    );

    if (!activeGuard) {
      return true;
    }
    // Get Request Object
    const request = context.switchToHttp().getRequest<TExtendedRequest>();

    // Get Bearer Token from Request Headers
    const bearerToken = this._getBearerToken(request);

    // Validate Token from keycloak server
    const authenticatedUserTokenOrFalse = await this._isValidKeycloakToken(
      bearerToken,
    );

    if (!authenticatedUserTokenOrFalse) {
      throw new UnauthorizedException();
    }

    const authenticatedUserToken = authenticatedUserTokenOrFalse;

    // Check if User exists in wallet
    const isUserInWallet = await this._hyperledgerFabricSerice.isUserExists(
      authenticatedUserToken.user_identity_label,
    );
    if (!isUserInWallet) {
      throw new UnauthorizedException();
    }
    // Inject Authenticated User
    request[IS_USER_AUTHENTICATED] = true;
    request[AUTHENTICATED_USER_INFO] = authenticatedUserToken;
    return true;
  }

  /*
   * Private Method to Validate token issued by keycloak
   * This is done by validating the cryptographic materials (offline)
   * Then calling the userinfo entrypoint to get user info
   */
  private async _isValidKeycloakToken(bearerToken: string | undefined) {
    if (!bearerToken) {
      this._logger.error('No Bearer Token was provided');
      return false;
    }
    const grantManager = this._keycloak.grantManager;

    try {
      // Create Grant
      const { access_token: accessToken } = await grantManager.createGrant({
        access_token: bearerToken as unknown as Token,
      });
      if (!accessToken) {
        return false;
      }

      // Validate Token Online
      await grantManager.validateToken(accessToken, 'Bearer');

      // Get User Info if the authentication strategy is Online
      const authenticatedUserInfo = await grantManager.userInfo<
        Token,
        TDecodedUserToken
      >(accessToken);
      return authenticatedUserInfo;
    } catch (ex) {
      this._logger.error(ex);
      return false;
    }
  }

  private _getBearerToken(request: Request): string | undefined {
    const header = request.headers.authorization;

    if (
      header &&
      (header.indexOf('bearer ') === 0 || header.indexOf('Bearer ') === 0)
    ) {
      const accessToken = header.substring(7);
      return accessToken;
    }
  }
}
