export const AUTHENTICATED_USER_INFO = '_user_info';
export const IS_USER_AUTHENTICATED = '_is_user_auth';

// Metadata Used for decorators
export const META_PROTECTED = '_meta_protected';
