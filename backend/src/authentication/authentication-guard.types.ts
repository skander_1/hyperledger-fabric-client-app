import { Request } from 'express';
import {
  AUTHENTICATED_USER_INFO,
  IS_USER_AUTHENTICATED,
} from './authentication.constants';

export type TDecodedUserToken = {
  msp_id: string;
  sub: string;
  user_identity_label: string;
  email_verified: true;
  preferred_username: string;
};

export type TExtendedRequest = Request & {
  [AUTHENTICATED_USER_INFO]: TDecodedUserToken;
  [IS_USER_AUTHENTICATED]: boolean;
};
