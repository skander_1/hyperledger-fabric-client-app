import { registerAs } from '@nestjs/config';
import { z } from 'zod';

const keycloakConfigSchema = z
  .object({
    realm: z.string().min(1),
    'auth-server-url': z.string().min(1),
    'ssl-required': z.string().min(1),
    resource: z.string().min(1),
    secret: z.string().min(1),
    'confidential-port': z
      .string()
      .min(1)
      .transform((str) => parseInt(str)),
    'realm-public-key': z.string().min(1),
  })
  .strict();
type TKeycloakConfig = z.infer<typeof keycloakConfigSchema>;

function getKeycloakConfig(): TKeycloakConfig {
  return keycloakConfigSchema.parse({
    realm: process.env.KEYCLOAK_REALM,
    'auth-server-url': process.env.KEYCLOAK_AUTH_SERVER_URL,
    'ssl-required': process.env.KEYCLOAK_SSL_REQUIRED,
    resource: process.env.KEYCLOAK_RESOURCE,
    secret: process.env.KEYCLOAK_SECRET,
    'confidential-port': process.env.KEYCLOK_CONFIDENTIAL_PORT,
    'realm-public-key': process.env.KEYCLOAK_REALM_PUBLIC_KEY,
  });
}

export default registerAs('keycloak-config', getKeycloakConfig);
