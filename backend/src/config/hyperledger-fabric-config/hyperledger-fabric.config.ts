import { registerAs } from '@nestjs/config';
import { z } from 'zod';

const hyperledgerFabricSchema = z
  .object({
    channelName: z.string().min(1),
    chaincodeName: z.string().min(1),
    connectionProfileFilePath: z.string().min(1),
    appIdentityFilePath: z.string().min(1),
    adminEnrollmentId: z.string().min(1),
    adminEnrollmentSecret: z.string().min(1),
    adminIdentityLabel: z.string().min(1),
    mspId: z.string().min(1),
  })
  .strict();
type ThyperledgerFabricConfig = z.infer<typeof hyperledgerFabricSchema>;

function getHyperledgerFabricConfig(): ThyperledgerFabricConfig {
  return hyperledgerFabricSchema.parse({
    channelName: process.env.CHANNEL_NAME,
    chaincodeName: process.env.CHAINCODE_NAME,
    connectionProfileFilePath: process.env.CONNECTION_PROFILE_FILE_PATH,
    appIdentityFilePath: process.env.IDENTITY_FILE_PATH,
    mspId: process.env.MSPID,
    adminEnrollmentId: process.env.ADMIN_ENROLLMENT_ID,
    adminEnrollmentSecret: process.env.ADMIN_ENROLLMENT_SECRET,
    adminIdentityLabel: process.env.ADMIN_IDENTITY_LABEL,
  });
}

export default registerAs(
  'hyperledger-fabric-config',
  getHyperledgerFabricConfig,
);
