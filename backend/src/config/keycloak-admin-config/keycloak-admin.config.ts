import { registerAs } from '@nestjs/config';
import { z } from 'zod';

const keycloakAmdinConfigSchema = z
  .object({
    baseUrl: z.string().min(1),
    clientId: z.string().min(1),
    clientSecret: z.string().min(1),
    grantType: z.enum(['client_credentials']),
    realmName: z.string().min(1),
  })
  .strict();
type TKeycloakAdminConfig = z.infer<typeof keycloakAmdinConfigSchema>;

function getKeycloakAdminConfig(): TKeycloakAdminConfig {
  return keycloakAmdinConfigSchema.parse({
    baseUrl: process.env.KEYCLOAK_ADMIN_BASEURL,
    clientId: process.env.KEYCLOAK_ADMIN_CLIENT_ID,
    clientSecret: process.env.KEYCLOAK_ADMIN_CLIENT_SECRET,
    grantType: process.env.KEYCLOAK_ADMIN_GRANT_TYPE,
    realmName: process.env.KEYCLOAK_ADMIN_REALM_NAME,
  });
}

export default registerAs('keycloak-admin-config', getKeycloakAdminConfig);
