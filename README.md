# Architecture
![Architecture](./docs/architecture.svg "Architecture")

# Authentication Flow (Backend)
1. Get access token from request headers
2. Verify Access token
3. Get user info from keycloak
4. Get user identity label property and verify that it exists in the wallet 
